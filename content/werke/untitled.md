---
title: untitled
info: "2013/2014: Eine Kooperation von Claudia Roth (Malerei), Cathy Sharp (Choreografie) und Thomas C. Gass (Sound-Design). Tanz: Stefanie Fischer, Patricia Rotondaro, Michael Pascault"
images:
  - file: dsc5622
    info: "untitled | Foto: Peter Schnetz, Basel"
  - file: dsc5694
    info: "untitled | Foto: Peter Schnetz, Basel"
  - file: dsc5544
    info: "untitled | Foto: Peter Schnetz, Basel"
  - file: dsc5762
    info: "untitled | Foto: Peter Schnetz, Basel"
  - file: p1030012
    info: "untitled | Foto: Peter Schnetz, Basel"
---
