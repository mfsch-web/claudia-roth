---
title: Momentum 2
info: 2012, Oel auf Leinwand
images:
  - file: testbild
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: rote-serie-20117601web
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: rote-serie-20117595web
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: rote-serie-20117597web
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: rote-serie-20128798web
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: rote-serie-20128782web
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: rote-serie-20128794web
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: rote-serie-20128780web
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: rote-serie-20128778web
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: rote-serie-20128784web-1
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: rote-serie-20128786web-1
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: rote-serie-20128790web-1
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: rote-serie-20128788web-1
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: rote-serie-20128792web-1
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: rote-serie-20128796web
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: 1220128999bsweb
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: 1220128997bsweb
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: 1220129001bsweb
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: 1220129003bsweb
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: 1220129005bsweb
    info: 2012, Oel auf Leinwand, 150 x 150 cm
  - file: 1220128995bsweb
    info: 2012, Oel auf Leinwand, 150 x 150 cm
---
