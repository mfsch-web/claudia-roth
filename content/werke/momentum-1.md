---
title: Momentum 1
info: 2011, Oel auf Leinwand
images:
  - file: aa20117581bsweb
    info: 2011, Oel auf Leinwand, 150 x 150 cm
  - file: aa20117577bsweb
    info: 2011, Oel auf Leinwand, 150 x 150 cm
  - file: aa20117575bsweb
    info: 2011, Oel auf Leinwand, 150 x 150 cm
  - file: aa20117571bsweb
    info: 2011, Oel auf Leinwand, 150 x 150 cm
  - file: aa20117563bsweb
    info: 2011, Oel auf Leinwand, 150 x 150 cm
  - file: aa20117561bsweb
    info: 2011, Oel auf Leinwand, 150 x 150 cm
  - file: aa20117552bsweb
    info: 2011, Oel auf Leinwand, 150 x 150 cm
  - file: aa20117550bsweb
    info: 2011, Oel auf Leinwand, 150 x 150 cm
  - file: aa20117548bsweb
    info: 2011, Oel auf Leinwand, 200 x 150 cm
  - file: aa20117543bsweb
    info: 2011, Oel auf Leinwand, 200 x 150 cm
  - file: aa20117537bsweb
    info: 2011, Oel auf Leinwand, 200 x 150 cm
  - file: aa20117540bsweb
    info: 2011, Oel auf Leinwand, 200 x 150 cm
  - file: aa20117604bsweb
    info: 2011, Oel auf Leinwand, 200 x 150 cm
  - file: aa20117566bsweb
    info: 2011, Oel auf Leinwand, 150 x 150 cm
  - file: aa20117557bsweb
    info: 2011, Oel auf Leinwand, 150 x 150 cm
---
