---
title: Lebenslauf
main-dates:
  - year: 1955
    description: geboren, lebt und arbeitet in Basel
  - year: 1980
    description: Diplom Lehramt für bildende Kunst
  - year: seit 1981
    description: Lehrtätigkeit im gestalterischen Bereich
work-areas:
  - Malerei
exhibitions:
  - year: 2016
    description: Projektraum M54 Visarte-Basel und H95 Raum für Kultur
  - year: 2015
    description: Chelsea Galerie Laufen
  - year: 2007
    description: Chelsea Galerie Laufen
  - year: 2005
    description: Kunstverein Bad Säckingen
  - year: 2004
    description: Weil Stapflehus Kunstverein Weil
  - year: 2004
    description: Ausstellungsraum Klingental Basel
  - year: 2003
    description: Chelsea Galerie Laufen
  - year: 2000/02/03
    description: Beteiligung an der Regionale Basel
  - year: 2001
    description: Galerie Mäder Basel
  - year: 1999
    description: Galerie Mäder Basel
  - year: 1991
    description: Galerie Mäder Basel
  - year: 1990
    description: Galerie Severina Teucher Zürich
  - year: 1989
    description: Galerie Rothenburghaus Hofkirche Luzern
  - year: 1987
    description: Galerie Dönisch-Seidel Kleve Deutschland
  - year: 1986
    description: Ausstellungsraum Kaserne Basel
  - year: 1983–86
    description: "Ausstellungen mit der Künstlergruppe 84 (Palazzo Liestal, Kunsthaus Aarau, Künstlerhaus Solothurn)"
funding:
  - year: 2000
    description: Ankauf Kunstkredit Basel-Land
  - year: 1988
    description: künstlerische Gestaltung der UBS Riehen
  - year: 1988
    description: Basler Stipendium
  - year: 1987
    description: Eidgenössisches Stipendium
  - year: 1987
    description: Ankauf Bank Julius Bär Zürich
  - year: 1987
    description: Ankauf Kreditanstalt Kunstsammlung
  - year: 1987
    description: Ankauf SBG Junge Schweizerkunst
  - year: 1986
    description: Basler Stipendium
---

test
