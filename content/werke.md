---
title: Werke
works:
  - slug: momentum-14
    thumbnail: 1
  - slug: momentum-13
    thumbnail: 1
  - slug: momentum-12
    thumbnail: 1
  - slug: momentum-11
    thumbnail: 1
  - slug: momentum-10
    thumbnail: 1
  - slug: momentum-9
    thumbnail: 1
  - slug: momentum-8
    thumbnail: 1
  - slug: momentum-7
    thumbnail: 1
  - slug: momentum-6
    thumbnail: 1
  - slug: momentum-5
    thumbnail: 1
  - slug: momentum-4
    thumbnail: 7
  - slug: momentum-3
    thumbnail: 5
  - slug: momentum-2
    thumbnail: 1
  - slug: momentum-1
    thumbnail: 6
  - slug: untitled
    thumbnail: 1
  - slug: klang-gang
    thumbnail: 1
  - slug: fluegelnacht
  - slug: erinnerung
  - slug: la-danse
  - slug: les-nuits-blanches
  - slug: les-fleures-du-mal
---
