.DEFAULT_GOAL := site
.PHONY: site clean

SITE = public
TOOLS = tools

PANDOC_VERSION = 2.11.1.1
pandoc = $(TOOLS)/pandoc-$(PANDOC_VERSION)/bin/pandoc
pandoc_options = --from=markdown-auto_identifiers-smart --wrap=preserve --section-divs
SASS_VERSION = 1.29.0
sass = $(TOOLS)/dart-sass/sass

works_overview := $(patsubst content/werke/%.md,/werke/%,$(wildcard content/werke/*))
# note the trailing space in the grep string, used to exclude works with just a
# single picture since those have 'file' at the top level
works_detail := $(patsubst content/%,/%,$(shell for f in content/werke/*.md; do grep -c ' file:' $$f | xargs seq | sed "s~^~$${f%.md}/~"; done))
# here we have no space in the grep string so all images should be matched
works_images := $(shell for f in content/werke/*.md; do grep file: $$f | sed 's/^.*file:[[:space:]]*//'; done)

content = $(patsubst content/%.md,/%,$(wildcard content/*.md)) \
    $(works_overview) $(works_detail)
static = $(patsubst static/%,/%,$(wildcard static/*))
scripts = $(patsubst scripts/%,/js/%,$(wildcard scripts/*.js))
$(info content: $(content))

paths = $(addsuffix .html,$(content)) \
    $(patsubst %,/img/%-154.jpg,$(works_images)) \
    $(patsubst %,/img/%-520.jpg,$(works_images)) \
    /css/main.css \
    $(scripts) \
    $(static)

site: $(addprefix $(SITE),$(paths))

clean:
	rm -rf $(SITE) $(TOOLS)

$(pandoc):
	mkdir -p $(TOOLS)
	wget -O $(TOOLS)/pandoc.tar.gz https://github.com/jgm/pandoc/releases/download/$(PANDOC_VERSION)/pandoc-$(PANDOC_VERSION)-linux-amd64.tar.gz
	tar -xzf $(TOOLS)/pandoc.tar.gz -C $(TOOLS)

$(sass):
	mkdir -p $(TOOLS)
	wget -O $(TOOLS)/dart-sass.tar.gz https://github.com/sass/dart-sass/releases/download/$(SASS_VERSION)/dart-sass-$(SASS_VERSION)-linux-x64.tar.gz
	tar -xzf $(TOOLS)/dart-sass.tar.gz -C $(TOOLS)

$(addprefix $(SITE),$(static)): $(SITE)/%: static/%
	mkdir -p $(@D)
	cp $< $@

$(SITE)/css/main.css: styles/main.scss styles/_normalize.scss | $(sass)
	mkdir -p $(@D)
	$(sass) $< $@

$(SITE)/img/%-154.jpg: images/%.jpg
	mkdir -p $(@D)
	convert $< -geometry x154 -sharpen 0x0.5 $@

$(SITE)/img/%-520.jpg: images/%.jpg
	mkdir -p $(@D)
	convert $< -geometry x520 $@

$(SITE)/js/%: scripts/%
	mkdir -p $(@D)
	cp $< $@

# html files with their own templates (cv, kontakt)
$(SITE)/%.html: content/%.md templates/%.html templates/header.html templates/footer.html | $(pandoc)
	mkdir -p $(@D)
	$(pandoc) $(pandoc_options) --template=templates/$*.html $(subst /, --variable=nav-,/$*) --output=$@ $<

# overview of all works
$(SITE)/werke.html: content/werke.md $(wildcard content/werke/*.md) templates/works-all.html
	mkdir -p $(@D)
	$(pandoc) $(pandoc_options) --template=templates/works-all.html --variable=nav-werke --lua-filter=templates/add_series_info.lua --output=$@ $<


.SECONDEXPANSION:

# overview of a work series or single piece
$(patsubst %,$(SITE)%.html,$(works_overview)): $$(patsubst public/%.html,content/%.md,$$@) templates/works-overview.html
	mkdir -p $(@D)
	$(pandoc) $(pandoc_options) --template=templates/works-overview.html --metadata=slug:$(notdir $(@:.html=)) --lua-filter=templates/number_items.lua --variable=nav-werke --output=$@ $<

# detail view of a single image that is part of a series
stem = %
$(SITE)/werke/%.html: $$(patsubst public/$$(stem)/,content/$$(stem).md,$$(dir $$@)) templates/works-detail.html
	mkdir -p $(@D)
	$(pandoc) $(pandoc_options) --template=templates/works-detail.html --metadata=slug:$(patsubst %/,%,$(dir $*)) --metadata=current:$(notdir $*) --lua-filter=templates/number_items.lua --variable=nav-werke --output=$@ $<
