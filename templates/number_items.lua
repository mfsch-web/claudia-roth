function Meta(m)
  if m.images ~= nil then
    for i,img in pairs(m.images) do
      img.index = pandoc.MetaString(tostring(i))
      if m.current == tostring(i) then
        img.current = pandoc.MetaBool(true)
      end
    end
  end
  return m
end
