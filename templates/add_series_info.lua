function Meta(m)
  for i,w in pairs(m.works) do
    slug = pandoc.utils.stringify(w.slug)
    fn = "content/werke/" .. slug .. ".md"
    print(fn)
    f = io.open(fn)
    if f == nil then
        print("file not found: " .. fn)
    else
        doc = f:read("*all")
        f:close()
        doc = pandoc.read(doc)
        w.title = doc.meta.title
        if doc.meta.images ~= nil then
            w.count = pandoc.MetaString(tostring(#doc.meta.images))
            thumb = tonumber(pandoc.utils.stringify(w.thumbnail))
            w.thumbnail = doc.meta.images[thumb].file
        else
            w.thumbnail = doc.meta.file
        end
    end
  end
  return m
end
